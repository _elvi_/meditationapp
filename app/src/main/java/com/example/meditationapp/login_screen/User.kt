package com.example.meditationapp.login_screen

data class User(
    val email: String,
    val password: String
)
