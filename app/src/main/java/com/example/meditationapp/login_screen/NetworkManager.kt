package com.example.meditationapp.login_screen

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface NetworkManager {

    companion object {
        val instance = Retrofit.Builder()
            .baseUrl("http://mskko2021.mad.hakta.pro/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NetworkManager::class.java)
    }

    @POST("user/login")
    fun createUser (@Body user: User) : Call<UserResponse>
}