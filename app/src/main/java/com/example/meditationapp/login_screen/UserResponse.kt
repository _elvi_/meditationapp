package com.example.meditationapp.login_screen

data class UserResponse(
    val email: String,
    val password: String,
    val id: String,
    val nickName: String,
    val avatar : String,
    val token: String
)
