package com.example.meditationapp.login_screen

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.meditationapp.MainActivity
import com.example.meditationapp.databinding.ActivityLoginBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.signInButton.setOnClickListener {
            val email = binding.email.text.toString()
            val password = binding.password.text.toString()

            val user = User(email, password)

            if (email.isNotEmpty() && password.isNotEmpty() && email.contains("@", ignoreCase = true)) {
                NetworkManager.instance
                    .createUser(user)
                    .enqueue(object : Callback<UserResponse> {
                        override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                            val user = response.body()
                            Toast.makeText(this@LoginActivity, user.toString(), Toast.LENGTH_LONG).show()
                        }
                        override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                            t.printStackTrace()
                            Toast.makeText(this@LoginActivity, t.localizedMessage, Toast.LENGTH_LONG).show()
                        }
                    })
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            }
        }
    }
}