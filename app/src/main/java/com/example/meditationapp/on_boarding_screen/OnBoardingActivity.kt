package com.example.meditationapp.on_boarding_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.meditationapp.login_screen.LoginActivity
import com.example.meditationapp.databinding.ActivityOnBoardingBinding

class OnBoardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnBoardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.signInButton.setOnClickListener() {
            startActivity(
                Intent(this, LoginActivity::class.java)
            )
            finish()
        }
    }
}